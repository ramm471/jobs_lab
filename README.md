# jobs_lab

Application for searching and publishing jobs

## Getting Started

- generate models: flutter pub run build_runner build --delete-conflicting-outputs
- create native splash: flutter pub run flutter_native_splash:create
- generate launcher icon: flutter pub run flutter_launcher_icons:main