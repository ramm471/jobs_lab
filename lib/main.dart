import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:graphql/client.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/jobs/presentation/bloc/refresh_listener.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'core/navigation/jobs_lab_router_delegate.dart';
import 'core/navigation/listener/job_id_listener.dart';
import 'core/navigation/parser/jobs_lab_parser_route_strategy_helper.dart';
import 'core/navigation/parser/jobs_lab_route_info_parser.dart';
import 'core/service/jobs_service.dart';
import 'core/utils/style/theme.dart';

Future<void> main() async {
  //Bloc.observer = SimpleBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  ResponsiveSizingConfig.instance.setCustomBreakpoints(
    const ScreenBreakpoints(desktop: 800, tablet: 800, watch: 200),
  );
  runApp(const JobsAppMultiProvider());
}

class JobsAppMultiProvider extends StatelessWidget {
  const JobsAppMultiProvider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<GraphQLClient>(
          create: (context) => GraphQLClient(
            cache: GraphQLCache(),
            link: HttpLink(
              jobsUrl,
            ),
          ),
        ),
        Provider<JobsService>(
          create: (context) => JobsServiceImpl(
            client: context.read<GraphQLClient>(),
          ),
        ),
        Provider<JobIdListener>(
          create: (context) => JobIdListenerImpl(),
        ),
        Provider<JobsRefreshListener>(
          create: (context) => JobsRefreshListenerImpl(),
        ),
        Provider<ParserRouteStrategyHelper>(
          create: (context) => ParserRouteStrategyHelperImpl(),
        ),
        Provider<JobsLabRouteInformationParser>(
          create: (context) => JobsLabRouteInformationParser(
            parserRouteStrategyHelper: context.read<ParserRouteStrategyHelper>(),
          ),
        ),
        ListenableProvider<JobsLabRouterDelegate>(
          create: (context) => JobsLabRouterDelegate(
            jobIdListener: context.read<JobIdListener>(),
          ),
        ),
      ],
      child: const JobsLabApp(),
    );
  }
}

class JobsLabApp extends StatelessWidget {
  const JobsLabApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: (JobsLabLocalizations.of(context)?.appTitle).orEmpty,
      theme: JobsLabTheme.theme,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        JobsLabLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      routerDelegate: context.read<JobsLabRouterDelegate>(),
      routeInformationParser: context.read<JobsLabRouteInformationParser>(),
    );
  }
}
