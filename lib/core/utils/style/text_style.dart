import 'package:flutter/material.dart';

import 'colors.dart';

class JobsLabTextStyle {
  static TextStyle? headline4AccentTextStyle(BuildContext context) => Theme.of(context).textTheme.headline4?.copyWith(
        color: JobsLabColors.textAccentColor,
      );

  static TextStyle? headline4ErrorTextStyle(BuildContext context) => Theme.of(context).textTheme.headline4?.copyWith(
        color: JobsLabColors.errorColor,
      );

  static TextStyle? subtitle2AccentTextStyle(BuildContext context) => Theme.of(context).textTheme.subtitle2?.copyWith(
        color: JobsLabColors.textAccentColor,
      );
}
