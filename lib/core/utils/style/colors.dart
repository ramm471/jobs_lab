import 'package:flutter/material.dart';

class JobsLabColors {
  static get primaryColor => const Color(0xff0F202D);

  static get onPrimaryColor => const Color(0xff29B973);

  static get accentColor => const Color(0xff2A7ED2);

  static get errorColor => const Color(0xffD64292);

  static get primaryColorDark => const Color(0xff09141C);

  static get backgroundColor => const Color(0xff172B3A);

  static get onBackgroundColor => const Color(0xff9FA6AB);

  static get disabledColor => const Color(0xffFAFAFA);

  static get dividerColor => const Color(0xffFAFAFA);

  static get textPrimaryColor => Colors.white;

  static get textSecondaryColor => Colors.grey;

  static get textAccentColor => onPrimaryColor;
}
