import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

class JobsLabTheme {
  static get theme {
    final originalTheme = ThemeData.dark();
    final originalTextTheme = originalTheme.textTheme;

    return originalTheme.copyWith(
        primaryColor: JobsLabColors.primaryColor,
        primaryColorDark: JobsLabColors.primaryColorDark,
        backgroundColor: JobsLabColors.backgroundColor,
        dividerColor: JobsLabColors.dividerColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: JobsLabColors.backgroundColor,
        canvasColor: JobsLabColors.primaryColorDark,
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            primary: JobsLabColors.onPrimaryColor,
            onSurface: JobsLabColors.disabledColor,
            textStyle: GoogleFonts.sourceSerifPro(
              textStyle: originalTextTheme.headline4?.copyWith(
                fontSize: 18,
              ),
            ),
            minimumSize: const Size(90, 40),
            padding: const EdgeInsets.symmetric(horizontal: 16),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2)),
            ),
          ),
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: JobsLabColors.accentColor,
          selectionColor: JobsLabColors.onPrimaryColor,
          selectionHandleColor: JobsLabColors.accentColor,
        ),
        textTheme: originalTextTheme.copyWith(
          headline1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline1?.copyWith(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
          ),
          headline2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline2?.copyWith(
              fontSize: 30,
            ),
          ),
          headline3: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline3?.copyWith(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          headline4: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.headline4?.copyWith(
              fontSize: 20,
            ),
          ),
          subtitle1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.subtitle1?.copyWith(
              fontSize: 18,
            ),
          ),
          subtitle2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.subtitle2?.copyWith(
              fontSize: 16,
            ),
          ),
          bodyText1: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.bodyText1?.copyWith(
              fontSize: 14,
            ),
          ),
          bodyText2: GoogleFonts.sourceSerifPro(
            textStyle: originalTextTheme.bodyText2?.copyWith(
              fontSize: 12,
            ),
          ),
        ),
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: JobsLabColors.primaryColor,
          onPrimary: JobsLabColors.onPrimaryColor,
          secondary: JobsLabColors.accentColor,
          onSecondary: JobsLabColors.primaryColor,
          error: JobsLabColors.errorColor,
          background: JobsLabColors.backgroundColor,
          onBackground: JobsLabColors.onBackgroundColor,
        ));
  }
}
