import 'package:flutter/cupertino.dart';

extension StringExtensions on String? {
  String get orEmpty {
    return this != null ? this! : "";
  }
}

T? safeCast<T>(dynamic obj) {
  try {
    return (obj as T);
  } on TypeError catch (e) {
    debugPrint('CastError when trying to cast $obj to $T!');
    return null;
  }
}
