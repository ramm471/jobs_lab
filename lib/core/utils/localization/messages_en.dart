import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

typedef MessageIfAbsent = Function(String messageStr, List args);

class MessageLookup extends MessageLookupByLibrary {
  @override
  get localeName => 'en';

  @override
  final messages = _notInlinedMessages(_notInlinedMessages);

  static _notInlinedMessages(_) => {
        "appTitle": MessageLookupByLibrary.simpleMessage("Jobs"),
        "jobsEmpty": MessageLookupByLibrary.simpleMessage("There are no jobs available"),
        "error": MessageLookupByLibrary.simpleMessage("Something went wrong, please try again later"),
        "success": MessageLookupByLibrary.simpleMessage("Job was successfully added to pending list"),
        "commitmentHint": MessageLookupByLibrary.simpleMessage("Choose a commitment"),
        "companyHint": MessageLookupByLibrary.simpleMessage("Choose a company"),
        "titleHint": MessageLookupByLibrary.simpleMessage("Enter title"),
        "descriptionHint": MessageLookupByLibrary.simpleMessage("Enter description"),
        "locationHint": MessageLookupByLibrary.simpleMessage("Enter location"),
        "emailHint": MessageLookupByLibrary.simpleMessage("Enter email"),
        "urlHint": MessageLookupByLibrary.simpleMessage("Enter apply url"),
        "titleValidationError": MessageLookupByLibrary.simpleMessage("Enter title"),
        "descriptionValidationError": MessageLookupByLibrary.simpleMessage("Enter description"),
        "emailValidationError": MessageLookupByLibrary.simpleMessage("Enter email"),
        "urlValidationError": MessageLookupByLibrary.simpleMessage("Enter apply url"),
        "saveButtonText": MessageLookupByLibrary.simpleMessage("Update"),
        "publishButtonText": MessageLookupByLibrary.simpleMessage("Publish"),
        "pushButtonText": MessageLookupByLibrary.simpleMessage("Push"),
        "pendingText": MessageLookupByLibrary.simpleMessage("Pending"),
        "jobPublishTitleText": MessageLookupByLibrary.simpleMessage("Publish Job"),
        "jobUpdateTitleText": MessageLookupByLibrary.simpleMessage("Update Job"),
        "jobTitleHint": MessageLookupByLibrary.simpleMessage("Title:"),
        "jobCommitmentHint": MessageLookupByLibrary.simpleMessage("Commitment:"),
        "jobCompanyHint": MessageLookupByLibrary.simpleMessage("Company:"),
        "jobLocationHint": MessageLookupByLibrary.simpleMessage("Location:"),
        "jobEmailHint": MessageLookupByLibrary.simpleMessage("Email:"),
        "jobApplyUrlHint": MessageLookupByLibrary.simpleMessage("Apply Url:"),
        "jobDescriptionHint": MessageLookupByLibrary.simpleMessage("Description:"),
        "successPublish": MessageLookupByLibrary.simpleMessage("Successfully published"),
      };
}
