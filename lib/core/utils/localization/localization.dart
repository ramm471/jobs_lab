import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import 'messages_all.dart';

class JobsLabLocalizations {
  final Locale _locale;

  JobsLabLocalizations({required Locale locale}) : _locale = locale;

  static Future<JobsLabLocalizations> load(Locale locale) {
    return initializeMessages(locale.toString()).then((_) {
      return JobsLabLocalizations(locale: locale);
    });
  }

  static JobsLabLocalizations? of(BuildContext context) {
    return Localizations.of<JobsLabLocalizations>(context, JobsLabLocalizations);
  }

  String get appTitle => Intl.message(
        'Jobs',
        name: 'appTitle',
        args: [],
        locale: _locale.toString(),
      );

  String get jobsEmpty => Intl.message(
        'There are no jobs available',
        name: 'jobsEmpty',
        args: [],
        locale: _locale.toString(),
      );

  String get error => Intl.message(
        'Something went wrong, please try again later.',
        name: 'error',
        args: [],
        locale: _locale.toString(),
      );

  String get success => Intl.message(
        'Job was successfully added to pending list',
        name: 'success',
        args: [],
        locale: _locale.toString(),
      );

  String get successPublish => Intl.message(
        'Successfully published',
        name: 'successPublish',
        args: [],
        locale: _locale.toString(),
      );

  String get commitmentHint => Intl.message(
        'Choose a commitment',
        name: 'commitmentHint',
        args: [],
        locale: _locale.toString(),
      );

  String get companyHint => Intl.message(
        'Choose a company',
        name: 'companyHint',
        args: [],
        locale: _locale.toString(),
      );

  String get titleHint => Intl.message(
        'Enter title',
        name: 'titleHint',
        args: [],
        locale: _locale.toString(),
      );

  String get locationHint => Intl.message(
        'Enter location',
        name: 'locationHint',
        args: [],
        locale: _locale.toString(),
      );

  String get descriptionHint => Intl.message(
        'Enter description',
        name: 'descriptionHint',
        args: [],
        locale: _locale.toString(),
      );

  String get emailHint => Intl.message(
        'Enter email',
        name: 'emailHint',
        args: [],
        locale: _locale.toString(),
      );

  String get urlHint => Intl.message(
        'Enter apply url',
        name: 'urlHint',
        args: [],
        locale: _locale.toString(),
      );

  String get titleValidationError => Intl.message(
        'Enter title',
        name: 'titleValidationError',
        args: [],
        locale: _locale.toString(),
      );

  String get descriptionValidationError => Intl.message(
        'Enter description',
        name: 'descriptionValidationError',
        args: [],
        locale: _locale.toString(),
      );

  String get locationValidationError => Intl.message(
        'Enter location',
        name: 'locationValidationError',
        args: [],
        locale: _locale.toString(),
      );

  String get emailValidationError => Intl.message(
        'Enter email',
        name: 'emailValidationError',
        args: [],
        locale: _locale.toString(),
      );

  String get urlValidationError => Intl.message(
        'Enter apply url',
        name: 'urlValidationError',
        args: [],
        locale: _locale.toString(),
      );

  String get saveButtonText => Intl.message(
        'Update',
        name: 'saveButtonText',
        args: [],
        locale: _locale.toString(),
      );

  String get publishButtonText => Intl.message(
        'Publish',
        name: 'publishButtonText',
        args: [],
        locale: _locale.toString(),
      );

  String get pushButtonText => Intl.message(
        'Push',
        name: 'pushButtonText',
        args: [],
        locale: _locale.toString(),
      );

  String get pendingText => Intl.message(
        'Pending',
        name: 'pendingText',
        args: [],
        locale: _locale.toString(),
      );

  String get jobsText => Intl.message(
        'Jobs',
        name: 'jobsText',
        args: [],
        locale: _locale.toString(),
      );

  String get jobPublishTitleText => Intl.message(
        'Publish Job',
        name: 'jobPublishTitleText',
        args: [],
        locale: _locale.toString(),
      );

  String get jobUpdateTitleText => Intl.message(
        'Update Job',
        name: 'jobUpdateTitleText',
        args: [],
        locale: _locale.toString(),
      );

  String get jobTitleHint => Intl.message(
        'Title:',
        name: 'jobTitleHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobCommitmentHint => Intl.message(
        'Commitment:',
        name: 'jobCommitmentHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobCompanyHint => Intl.message(
        'Company:',
        name: 'jobCompanyHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobLocationHint => Intl.message(
        'Location:',
        name: 'jobLocationHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobEmailHint => Intl.message(
        'Email:',
        name: 'jobEmailHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobApplyUrlHint => Intl.message(
        'Apply Url:',
        name: 'jobApplyUrlHint',
        args: [],
        locale: _locale.toString(),
      );

  String get jobDescriptionHint => Intl.message(
        'Description:',
        name: 'jobDescriptionHint',
        args: [],
        locale: _locale.toString(),
      );
}

class JobsLabLocalizationsDelegate extends LocalizationsDelegate<JobsLabLocalizations> {
  Locale? _locale;

  @override
  Future<JobsLabLocalizations> load(Locale locale) {
    debugPrint("JobsLabLocalizationsDelegate load $locale");
    _locale = locale;
    return JobsLabLocalizations.load(locale);
  }

  @override
  bool shouldReload(JobsLabLocalizationsDelegate old) {
    debugPrint("JobsLabLocalizationsDelegate shouldReload $old");
    return old._locale != _locale;
  }

  @override
  bool isSupported(Locale locale) {
    debugPrint("JobsLabLocalizationsDelegate isSupported $locale");
    return locale.languageCode.toLowerCase().contains("en");
  }
}
