import 'package:flutter/widgets.dart';

class JobsLabKeys {
  static const appKey = Key('__jobs_lab_app__');

  //jobs
  static const jobsKey = Key('__jobs_screen_app__');
  static const jobsScaffoldKey = Key('__jobs_scaffold_screen_app__');
  static const jobsScreenWidgetKey = Key('__jobs_screen_widget_screen_app__');
  static const jobsAppBarKey = Key('__jobs_app_bar_screen_app__');
  static const jobsAppBarTextKey = Key('__jobs_app_bar_text_screen_app__');
  static const jobsResponsiveBuilderKey = Key('__jobs_responsive_builder_screen_app__');
  static const jobsBlocProviderKey = Key('__jobs_bloc_provider_screen_app__');
  static const jobsBlocKey = Key('__jobs_bloc_screen_app__');
  static const jobsWidgetKey = Key('__jobs_widget_screen_app__');
  static const jobsSliverWidgetKey = Key('__jobs_sliver_widget_screen_app__');
  static const jobsContainerKey = Key('__jobs_container_screen_app__');
  static const jobsSliverItemKey = Key('__jobs_sliver_item_screen_app__');
  static const jobsSliverPendingItemKey = Key('__jobs_sliver_pending_item_screen_app__');

  static jobsItem(String id) => Key('jobs_item__${id}__');

  static jobsItemTitle(String id) => Key('jobs_item_title__${id}__');

  static jobsItemSubTitle(String id) => Key('jobs_item_subtitle__${id}__');

  //job
  static jobKey(String id) => Key('__job_screen_app__${id}__');

  static jobScaffoldKey(String id) => Key('__job_scaffold_screen_app__${id}__');

  static jobAppBarKey(String id) => Key('__job_app_bar_screen_app__${id}__');

  static jobAppBarTextKey(String id) => Key('__job_app_bar_text_screen_app__${id}__');

  static jobResponsiveBuilderKey(String id) => Key('__job_responsive_builder_screen_app__${id}__');

  static jobBlocProviderKey(String id) => Key('__job_bloc_provider_screen_app__${id}__');

  static jobScreenWidgetKey(String id) => Key('__job_screen_widget_screen_app__${id}__');

  static jobBlocKey(String id) => Key('__job_bloc_screen_app__${id}__');

  static jobWidgetKey(String id) => Key('__job_widget_screen_app__${id}__');

  static jobCommitmentKey(String id) => Key('__job_commitment_screen_app__${id}__');

  static jobCompaniesKey(String id) => Key('__job_companies_screen_app__${id}__');

  static jobTitleWidgetKey(String id) => Key('__job_title_widget_screen_app__${id}__');

  static jobDescriptionWidgetKey(String id) => Key('__job_description_widget_screen_app__${id}__');

  static jobLocationWidgetKey(String id) => Key('__job_location_widget_screen_app__${id}__');

  static jobEmailWidgetKey(String id) => Key('__job_email_widget_screen_app__${id}__');

  static jobUrlWidgetKey(String id) => Key('__job_url_widget_screen_app__${id}__');

  static jobInputsWidgetKey(String id) => Key('__job_inputs_widget_screen_app__${id}__');

  static const jobCommitmentDropDownKey = Key('__job_commitment_drop_down_screen_app__');
  static const jobCompaniesDropDownKey = Key('__job_companies_drop_down_screen_app__');
  static const jobTitleKey = Key('__job_title_screen_app__');
  static const jobDescriptionKey = Key('__job_description_screen_app__');
  static const jobLocationKey = Key('__job_location_screen_app__');
  static const jobEmailKey = Key('__job_email_screen_app__');
  static const jobUrlKey = Key('__job_url_screen_app__');
}
