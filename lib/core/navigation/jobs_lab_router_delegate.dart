import 'dart:async';
import 'package:flutter/material.dart';
import 'package:jobs_lab/core/navigation/page/job_page.dart';
import 'package:jobs_lab/core/navigation/page/jobs_page.dart';
import 'package:jobs_lab/core/navigation/parser/jobs_lab_route_info_parser.dart';
import 'listener/job_id_listener.dart';

class JobsLabRouterDelegate extends RouterDelegate<JobsLabRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<JobsLabRoutePath> {
  late final StreamSubscription? _jobIdListenerSubscription;
  String? _selectedJobId;

  JobsLabRouterDelegate({
    required JobIdListener jobIdListener,
  }) {
    _jobIdListenerSubscription = jobIdListener.selectedValue.listen((id) {
      _onJobItemUpdated(id);
    });
  }

  @override
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  void dispose() {
    debugPrint("dispose");
    _jobIdListenerSubscription?.cancel();
    super.dispose();
  }

  @override
  JobsLabRoutePath get currentConfiguration {
    if (_selectedJobId != null) {
      return JobsLabRoutePath.job(jobId: _selectedJobId);
    }
    return JobsLabRoutePath.jobs();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("RouterDelegate build");
    return Navigator(
      key: navigatorKey,
      pages: _buildPages(),
      onPopPage: _onPopPage,
    );
  }

  @override
  Future<void> setNewRoutePath(configuration) async {
    debugPrint("RouterDelegate setNewRoutePath $configuration");
    _selectedJobId = configuration.jobId;
  }

  List<Page> _buildPages() {
    debugPrint("RouterDelegate _createPages");
    final id = _selectedJobId;
    return [
      const JobsPage(),
      if (id != null) JobPage(id: id),
    ];
  }

  bool _onPopPage(Route route, dynamic result) {
    debugPrint("RouterDelegate _onPopPage");
    if (!route.didPop(result)) return false;
    if (_selectedJobId != null) {
      _selectedJobId = null;
    }
    notifyListeners();
    return true;
  }

  void _onJobItemUpdated(String? id) {
    _selectedJobId = id;
    notifyListeners();
  }
}
