import 'package:flutter/cupertino.dart';

import 'jobs_lab_route_info_parser.dart';

abstract class ParserRouteStrategy {
  bool isRoutePath(Uri? uri);

  JobsLabRoutePath? mapToRoutePath(Uri? uri);

  bool isRouteInformation(JobsLabRoutePath configuration);

  RouteInformation? restoreRouteInformation(JobsLabRoutePath configuration);
}
