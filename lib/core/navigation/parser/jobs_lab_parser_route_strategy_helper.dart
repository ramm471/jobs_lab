import 'package:jobs_lab/core/navigation/parser/strategy/job_parser_route_strategy.dart';
import 'package:jobs_lab/core/navigation/parser/strategy/jobs_parser_route_strategy.dart';

import 'jobs_lab_parser_route_strategy.dart';

abstract class ParserRouteStrategyHelper {
  void addStrategy(ParserRouteStrategy strategy);

  List<ParserRouteStrategy> getAllStrategies();

  ParserRouteStrategy getDefaultStrategy();
}

class ParserRouteStrategyHelperImpl implements ParserRouteStrategyHelper {
  final List<ParserRouteStrategy> _strategies = [
    JobsParserRouteStrategy(),
    JobParserRouteStrategy(),
  ];

  @override
  void addStrategy(ParserRouteStrategy strategy) {
    if (!_strategies.contains(strategy)) {
      _strategies.add(strategy);
    }
  }

  @override
  List<ParserRouteStrategy> getAllStrategies() => _strategies;

  @override
  ParserRouteStrategy getDefaultStrategy() {
    return JobsParserRouteStrategy();
  }
}
