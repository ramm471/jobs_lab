import 'package:flutter/cupertino.dart';

import '../jobs_lab_parser_route_strategy.dart';
import '../jobs_lab_route_info_parser.dart';

class JobParserRouteStrategy implements ParserRouteStrategy {
  @override
  bool isRoutePath(Uri? uri) {
    return _isJobUri(uri);
  }

  @override
  JobsLabRoutePath? mapToRoutePath(Uri? uri) {
    if (uri != null && _isJobUri(uri)) {
      return JobsLabRoutePath.job(jobId: uri.pathSegments[_jobPathSegmentNumber]);
    }
    return null;
  }

  @override
  RouteInformation? restoreRouteInformation(JobsLabRoutePath configuration) {
    if (isRouteInformation(configuration)) {
      return RouteInformation(location: '$jobUrl/${configuration.jobId}');
    }
    return null;
  }

  bool _isJobUri(Uri? uri) {
    debugPrint("JobParserRouteStrategy _isJobUri $uri");
    return uri != null &&
        uri.pathSegments.length >= 2 &&
        uri.pathSegments[_jobUrlPathSegmentNumber] == jobUrl.replaceAll("/", "");
  }

  @override
  bool isRouteInformation(JobsLabRoutePath configuration) {
    debugPrint("JobParserRouteStrategy isRouteInformation $configuration");
    return configuration.jobId != null;
  }
}

const String jobUrl = "/job";
const int _jobPathSegmentNumber = 1;
const int _jobUrlPathSegmentNumber = 0;
