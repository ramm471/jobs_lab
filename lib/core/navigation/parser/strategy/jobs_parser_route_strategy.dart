import 'package:flutter/cupertino.dart';

import '../jobs_lab_parser_route_strategy.dart';
import '../jobs_lab_route_info_parser.dart';

class JobsParserRouteStrategy implements ParserRouteStrategy {
  @override
  bool isRoutePath(Uri? uri) {
    return _isJobsUri(uri);
  }

  @override
  JobsLabRoutePath? mapToRoutePath(Uri? uri) {
    if (uri != null && _isJobsUri(uri)) {
      return JobsLabRoutePath.jobs();
    }
    return null;
  }

  @override
  RouteInformation? restoreRouteInformation(JobsLabRoutePath configuration) {
    if (isRouteInformation(configuration)) {
      return const RouteInformation(location: jobsUrl);
    }
    return null;
  }

  bool _isJobsUri(Uri? uri) {
    debugPrint("JobsParserRouteStrategy _isJobsUri $uri");
    return uri != null && uri.pathSegments.isEmpty;
  }

  @override
  bool isRouteInformation(JobsLabRoutePath configuration) {
    debugPrint("JobsParserRouteStrategy isRouteInformation $configuration");
    return configuration.jobId == null;
  }
}

const String jobsUrl = "/jobs";
