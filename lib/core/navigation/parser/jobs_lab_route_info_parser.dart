import 'package:flutter/material.dart';
import 'package:jobs_lab/core/navigation/parser/strategy/jobs_parser_route_strategy.dart';
import 'package:jobs_lab/core/utils/utils.dart';

import 'jobs_lab_parser_route_strategy_helper.dart';

class JobsLabRouteInformationParser extends RouteInformationParser<JobsLabRoutePath> {
  final ParserRouteStrategyHelper _parserRouteStrategyHelper;

  const JobsLabRouteInformationParser({
    required ParserRouteStrategyHelper parserRouteStrategyHelper,
  }) : _parserRouteStrategyHelper = parserRouteStrategyHelper;

  @override
  Future<JobsLabRoutePath> parseRouteInformation(RouteInformation routeInformation) async {
    final uri = Uri.tryParse(routeInformation.location.orEmpty);
    debugPrint("parseRouteInformation $uri");
    return _parserRouteStrategyHelper
            .getAllStrategies()
            .firstWhere(
              (strategy) => strategy.isRoutePath(uri),
              orElse: () => _parserRouteStrategyHelper.getDefaultStrategy(),
            )
            .mapToRoutePath(uri) ??
        JobsLabRoutePath.jobs();
  }

  @override
  RouteInformation? restoreRouteInformation(JobsLabRoutePath configuration) {
    debugPrint("restoreRouteInformation ${configuration.jobId}");
    return _parserRouteStrategyHelper
            .getAllStrategies()
            .firstWhere(
              (strategy) => strategy.isRouteInformation(configuration),
              orElse: () => _parserRouteStrategyHelper.getDefaultStrategy(),
            )
            .restoreRouteInformation(configuration) ??
        const RouteInformation(location: jobsUrl);
  }
}

class JobsLabRoutePath {
  final String? jobId;

  JobsLabRoutePath.jobs() : jobId = null;

  JobsLabRoutePath.job({required this.jobId});
}
