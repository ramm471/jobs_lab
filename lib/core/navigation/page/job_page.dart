import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/screens/job/presentation/job_screen.dart';

class JobPage extends Page {
  final String _id;

  const JobPage({
    required String id,
  })  : _id = id,
        super(key: const ValueKey("JobPage"));

  @override
  Route createRoute(BuildContext context) {
    debugPrint("JobPage createRoute");
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        debugPrint("JobPage builder");
        return JobScreen(
          key: JobsLabKeys.jobKey(_id),
          id: _id,
        );
      },
    );
  }
}
