import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/screens/jobs/presentation/jobs_screen.dart';

class JobsPage extends Page {
  const JobsPage() : super(key: const ValueKey("JobsPage"));

  @override
  Route createRoute(BuildContext context) {
    debugPrint("JobsPage createRoute");
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        debugPrint("JobsPage builder");
        return const JobsScreen(
          key: JobsLabKeys.jobsKey,
        );
      },
    );
  }
}
