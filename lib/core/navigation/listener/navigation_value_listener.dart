import 'dart:async';

abstract class NavigationValueProvider<V> {
  Stream<V?> get selectedValue;

  void selectValue(V? id);
}

class NavigationValueProviderImpl<V> extends NavigationValueProvider<V> {
  final StreamController<V?> _streamController = StreamController<V?>();

  @override
  Stream<V?> get selectedValue => _streamController.stream;

  @override
  void selectValue(V? id) {
    _streamController.add(id);
  }
}
