import 'navigation_value_listener.dart';

abstract class JobIdListener extends NavigationValueProviderImpl<String> {}

class JobIdListenerImpl extends JobIdListener {}
