import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';

class TextFormInputDecoration extends InputDecoration {
  TextFormInputDecoration({required String? hintText})
      : super(
          hintText: hintText,
          isDense: true,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: JobsLabColors.onBackgroundColor,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: JobsLabColors.accentColor,
            ),
          ),
        );
}
