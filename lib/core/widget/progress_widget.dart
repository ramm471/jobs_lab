import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';

class CircularProgressWidget extends StatelessWidget {
  const CircularProgressWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      backgroundColor: Colors.transparent,
      color: JobsLabColors.onPrimaryColor,
    );
  }
}
