import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/style/text_style.dart';

abstract class SnackBarHelper {
  static void showSuccess(BuildContext context, String message) {
    _showSnackBar(context, message, JobsLabColors.primaryColorDark);
  }

  static void showError(BuildContext context, String message) {
    _showSnackBar(context, message, JobsLabColors.errorColor);
  }

  static void _showSnackBar(BuildContext context, String message, Color backgroundColor) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: JobsLabTextStyle.subtitle2AccentTextStyle(context),
        ),
        backgroundColor: backgroundColor,
      ),
    );
  }
}
