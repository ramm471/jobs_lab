import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:graphql/client.dart';

import 'jobs_queries.dart';

abstract class JobsService {
  Future<QueryResult> loadJobs();

  Future<QueryResult> pushJob(Map<String, dynamic> variables);

  Future<QueryResult> loadCommitments();

  Future<QueryResult> loadCompanies();
}

class JobsServiceImpl implements JobsService {
  final GraphQLClient _client;

  JobsServiceImpl({
    required GraphQLClient client,
  }) : _client = client;

  @override
  Future<QueryResult> loadJobs() async {
    debugPrint("loadJobs");

    final QueryResult result = await _client.query(
      QueryOptions(
        document: gql(readJobs),
      ),
    );
    return result;
  }

  @override
  Future<QueryResult> loadCommitments() async {
    debugPrint("loadCommitments");

    final QueryResult result = await _client.query(
      QueryOptions(
        document: gql(readCommitments),
      ),
    );
    return result;
  }

  @override
  Future<QueryResult> loadCompanies() async {
    debugPrint("loadCompanies");

    final QueryResult result = await _client.query(
      QueryOptions(
        document: gql(readCompanies),
      ),
    );
    return result;
  }

  @override
  Future<QueryResult> pushJob(Map<String, dynamic> variables) async {
    debugPrint("pushJobs $variables");

    final result = await _client.mutate(
      MutationOptions(
        document: gql(postJob),
        variables: variables,
      ),
    );
    return result;
  }
}

const String jobsUrl = "https://api.graphql.jobs/";
