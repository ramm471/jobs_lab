const String readJobs = r'''
  query readJobs($type: String, $slug: String) {
      jobs(input: { type: $type, slug: $slug }) {
        id
        title
        commitment {
          id
          title
        }
        company {
          id
          name
        }
        locationNames
        userEmail
        applyUrl
        description
      }
  }
''';

const String readCommitments = r'''
  query readCommitments() {
      commitments {
        id
        title
      }
  }
''';

const String readCompanies = r'''
  query readCompanies() {
      companies {
        id
        name
      }
  }
''';

const String postJob = r'''
  mutation postJob($title: String!, $commitmentId: ID!, $companyName: String!, $locationNames: String!, $userEmail: String!, $description: String!, $applyUrl: String!) {
      postJob(input: { title: $title, commitmentId: $commitmentId, companyName: $companyName, locationNames: $locationNames, userEmail: $userEmail, description: $description, applyUrl: $applyUrl }) {
        id
        title
        commitment {
          id
          title
        }
        company {
          id
          name
        }
        locationNames
        userEmail
        applyUrl
        description
      }
  }
''';
