import 'dart:collection';
import 'dart:convert';
import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint, kIsWeb;
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseCache<E> {
  Future<List<E>> getEntities();

  Future<void> setEntities(List<E> entities);
}

abstract class BaseCacheImpl<E> implements BaseCache<E> {
  final E Function(Map<String, dynamic> json) _fromJson;
  final String _entityKey;

  BaseCacheImpl({
    required String entityKey,
    required E Function(Map<String, dynamic> json) fromJson,
  })  : _entityKey = entityKey,
        _fromJson = fromJson;

  @override
  Future<List<E>> getEntities() async {
    final result = <E>[];
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final json = prefs.getString(_entityKey) ?? "";
      final list = jsonDecode(json)[_entityKey];
      final List<Map<String, dynamic>>? entitiesJson = list != null ? List.from(list) : null;
      final entities = entitiesJson?.map((e) => _fromJson(e)).toList() ?? [];
      result.addAll(entities);
    } catch (e) {
      debugPrint("BaseCacheImpl getEntities error: $e");
    }
    debugPrint("BaseCacheImpl getEntities success");
    return result;
  }

  @override
  Future<void> setEntities(List<E> entities) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      final String json = jsonEncode(LinkedHashSet.from(entities).toList());
      final Map<String, String> jobsJson = {"\"$_entityKey\"": json};
      await prefs.setString(_entityKey, jobsJson.toString());
      debugPrint("BaseCacheImpl setEntities success");
    } catch (e) {
      debugPrint("BaseCacheImpl setEntities error: $e");
    }
  }
}
