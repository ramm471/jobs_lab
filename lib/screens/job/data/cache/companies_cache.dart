import 'dart:core';
import 'package:jobs_lab/screens/jobs/data/entity/company_entity.dart';

import '../../../../core/cache/base_cache.dart';

abstract class CompaniesCache extends BaseCacheImpl<CompanyEntity> {
  CompaniesCache()
      : super(
          entityKey: _companiesKey,
          fromJson: (e) => CompanyEntity.fromJson(e),
        );
}

class CompaniesCacheImpl extends CompaniesCache {}

const String _companiesKey = "_COMPANIES_KEY";
