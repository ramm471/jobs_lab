import 'dart:core';

import 'package:jobs_lab/screens/jobs/data/entity/commitment_entity.dart';

import '../../../../core/cache/base_cache.dart';

abstract class CommitmentsCache extends BaseCacheImpl<CommitmentEntity> {
  CommitmentsCache()
      : super(
          entityKey: _commitmentsKey,
          fromJson: (e) => CommitmentEntity.fromJson(e),
        );
}

class CommitmentsCacheImpl extends CommitmentsCache {}

const String _commitmentsKey = "_COMMITMENTS_KEY";
