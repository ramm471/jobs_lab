import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:graphql/client.dart';
import 'package:jobs_lab/screens/jobs/data/entity/commitment_entity.dart';
import 'package:jobs_lab/screens/jobs/data/entity/company_entity.dart';

abstract class JobParser {
  Future<List<CommitmentEntity>> parseCommitmentsResponse(QueryResult result);

  Future<List<CompanyEntity>> parseCompaniesResponse(QueryResult result);
}

class JobParserImpl implements JobParser {
  @override
  Future<List<CommitmentEntity>> parseCommitmentsResponse(QueryResult result) async {
    debugPrint("parseCommitmentsResponse");
    final List<CommitmentEntity> entities = [];
    if (result.hasException) {
      debugPrint("parseCommitmentsResponse error ${result.exception?.toString()} error: ${result.exception}");
      throw Exception("parseCommitmentsResponse error ${result.exception?.toString()}");
    } else {
      debugPrint("parseCommitmentsResponse parsing");
      final jobsJson = result.data?['commitments'] as List<dynamic>?;
      final List<CommitmentEntity> parsed =
          jobsJson?.map((e) => _tryParseCommitmentJson(e)).whereType<CommitmentEntity>().toList() ?? [];
      entities.addAll(parsed);
      debugPrint("parseCommitmentsResponse entities size: ${entities.length}");
    }
    return entities;
  }

  @override
  Future<List<CompanyEntity>> parseCompaniesResponse(QueryResult result) async {
    debugPrint("parseCompaniesResponse");
    final List<CompanyEntity> entities = [];
    if (result.hasException) {
      debugPrint("parseCompaniesResponse error ${result.exception?.toString()} error: ${result.exception}");
      throw Exception("parseCompaniesResponse error ${result.exception?.toString()}");
    } else {
      debugPrint("parseCompaniesResponse parsing");
      final json = result.data?['companies'] as List<dynamic>?;
      final List<CompanyEntity> parsedJobs =
          json?.map((e) => _tryParseCompanyJson(e)).whereType<CompanyEntity>().toList() ?? [];
      entities.addAll(parsedJobs);
      debugPrint("parseCompaniesResponse entities size: ${entities.length}");
    }
    return entities;
  }

  CommitmentEntity? _tryParseCommitmentJson(dynamic value) {
    try {
      return CommitmentEntity.fromJson(Map<String, dynamic>.from(value));
    } catch (e) {
      debugPrint("_tryParseCommitmentJson error: $e");
      return null;
    }
  }

  CompanyEntity? _tryParseCompanyJson(dynamic value) {
    try {
      return CompanyEntity.fromJson(Map<String, dynamic>.from(value));
    } catch (e) {
      debugPrint("_tryParseCompanyJson error: $e");
      return null;
    }
  }
}
