import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint;
import 'package:graphql/client.dart';
import 'package:jobs_lab/core/service/jobs_service.dart';
import 'package:jobs_lab/screens/job/data/cache/commitments_cache.dart';
import 'package:jobs_lab/screens/job/data/cache/companies_cache.dart';
import 'package:jobs_lab/screens/job/domain/repository/job_repository.dart';
import 'package:jobs_lab/screens/jobs/data/entity/commitment_entity.dart';
import 'package:jobs_lab/screens/jobs/data/entity/company_entity.dart';

import 'job_parser.dart';

class JobRepositoryImpl implements JobRepository {
  final JobsService _service;
  final CommitmentsCache _commitmentsCache;
  final CompaniesCache _companiesCache;
  final JobParser _parser;

  JobRepositoryImpl({
    required JobsService service,
    required CommitmentsCache commitmentsCache,
    required CompaniesCache companiesCache,
    required JobParser parser,
  })  : _service = service,
        _commitmentsCache = commitmentsCache,
        _companiesCache = companiesCache,
        _parser = parser;

  @override
  Future<List<CommitmentEntity>> loadCommitments() async {
    debugPrint("loadCommitments");

    final cachedEntities = await _commitmentsCache.getEntities();
    if (cachedEntities.isNotEmpty) {
      return cachedEntities;
    } else {
      final QueryResult result = await _service.loadCommitments();
      final entities = await _parser.parseCommitmentsResponse(result);
      _commitmentsCache.setEntities(entities);
      debugPrint("loadCommitments return ${entities.length} entities");
      return entities;
    }
  }

  @override
  Future<List<CompanyEntity>> loadCompanies() async {
    debugPrint("loadCompanies");

    final cachedEntities = await _companiesCache.getEntities();
    if (cachedEntities.isNotEmpty) {
      return cachedEntities;
    } else {
      final QueryResult result = await _service.loadCompanies();
      final entities = await _parser.parseCompaniesResponse(result);
      _companiesCache.setEntities(entities);
      debugPrint("loadCompanies return ${entities.length} entities");
      return entities;
    }
  }
}
