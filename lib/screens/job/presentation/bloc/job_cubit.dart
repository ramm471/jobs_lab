import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobs_lab/screens/job/domain/job_interactor.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/job_details_model.dart';
import 'package:uuid/uuid.dart';

import 'job_state.dart';

class JobCubit extends Cubit<JobState> {
  final String _id;
  final JobInteractor _jobInteractor;

  JobDetailsModel? _initialJob;
  JobDetailsModel _modifiedJob = const JobDetailsModel();
  List<CommitmentModel>? _commitments;
  List<CompanyModel>? _companies;

  JobCubit({
    required JobInteractor jobInteractor,
    required String id,
  })  : _jobInteractor = jobInteractor,
        _id = id,
        super(const JobLoadingState()) {
    loadJobDetails();
  }

  Future<void> loadJobDetails() async {
    debugPrint("loadJob");
    emit(const JobLoadingState());

    try {
      final loadedJob = await _jobInteractor.loadJob(_id);
      _initialJob = loadedJob;
      _modifiedJob = loadedJob;
      _commitments = await _jobInteractor.loadCommitments();
      _companies = await _jobInteractor.loadCompanies();
      emit(JobLoadedState(
        job: _modifiedJob,
        commitments: _commitments ?? [],
        companies: _companies ?? [],
        isSaveButtonEnabled: false,
      ));
    } catch (e) {
      emit(const JobErrorState());
    }
  }

  void selectCommitment(CommitmentModel commitment) {
    _modifiedJob = _modifiedJob.copyWith(commitment: commitment);
    _emitNewState();
  }

  void selectCompany(CompanyModel company) {
    _modifiedJob = _modifiedJob.copyWith(company: company);
    _emitNewState();
  }

  void selectTitle(String title) {
    final updatedModel = _modifiedJob.copyWith(title: title);
    _updateSaveButton(_modifiedJob, updatedModel);
    _modifiedJob = updatedModel;
  }

  void selectDescription(String description) {
    final updatedModel = _modifiedJob.copyWith(description: description);
    _updateSaveButton(_modifiedJob, updatedModel);
    _modifiedJob = updatedModel;
  }

  void selectLocation(String location) {
    final updatedModel = _modifiedJob.copyWith(location: location);
    _updateSaveButton(_modifiedJob, updatedModel);
    _modifiedJob = updatedModel;
  }

  void selectEmail(String email) {
    final updatedModel = _modifiedJob.copyWith(userEmail: email);
    _updateSaveButton(_modifiedJob, updatedModel);
    _modifiedJob = updatedModel;
  }

  void selectUrl(String url) {
    final updatedModel = _modifiedJob.copyWith(applyUrl: url);
    _updateSaveButton(_modifiedJob, updatedModel);
    _modifiedJob = updatedModel;
  }

  void save() async {
    debugPrint("save");
    await _jobInteractor.addPending(JobDetailsModel.toEntity(_modifiedJob).copyWith(
      isPending: true,
      id: (_id.isNotEmpty) ? _id : const Uuid().v4(),
    ));
    emit(const JobAddedState());
  }

  void _updateSaveButton(JobDetailsModel previous, JobDetailsModel current) {
    final previousState = state;
    final isPreviousSaveButtonEnabled = (previousState is JobLoadedState) ? previousState.isSaveButtonEnabled : false;
    final isSaveButtonEnabled = current != _initialJob && current.isNotEmpty;

    if (previous.isNotEmpty != current.isNotEmpty || isPreviousSaveButtonEnabled != isSaveButtonEnabled) {
      emit(JobLoadedState(
        job: current,
        commitments: _commitments ?? [],
        companies: _companies ?? [],
        isSaveButtonEnabled: isSaveButtonEnabled,
      ));
    }
  }

  void _emitNewState() {
    emit(JobLoadedState(
      job: _modifiedJob,
      commitments: _commitments ?? [],
      companies: _companies ?? [],
      isSaveButtonEnabled: _initialJob != _modifiedJob && _modifiedJob.isNotEmpty,
    ));
  }
}
