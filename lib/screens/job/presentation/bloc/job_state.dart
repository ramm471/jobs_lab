import 'package:equatable/equatable.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/job_details_model.dart';

abstract class JobState extends Equatable {
  const JobState() : super();

  @override
  List<Object?> get props => [];
}

class JobLoadingState extends JobState {
  const JobLoadingState() : super();
}

class JobAddedState extends JobState {
  const JobAddedState() : super();
}

class JobErrorState extends JobState {
  final String? error;

  const JobErrorState({this.error}) : super();

  @override
  List<Object?> get props => [error];

  @override
  String toString() {
    return 'JobErrorState{error: $error}';
  }
}

class JobLoadedState extends JobState {
  final JobDetailsModel job;
  final List<CommitmentModel> commitments;
  final List<CompanyModel> companies;
  final bool isSaveButtonEnabled;

  const JobLoadedState({
    required this.job,
    required this.commitments,
    required this.companies,
    required this.isSaveButtonEnabled,
  }) : super();

  @override
  List<Object> get props => [
        job,
        commitments,
        companies,
        isSaveButtonEnabled,
      ];

  @override
  String toString() {
    return 'JobLoadedState{job: $job, commitments: $commitments; companies: $companies; isSaveButtonEnabled: $isSaveButtonEnabled}';
  }
}
