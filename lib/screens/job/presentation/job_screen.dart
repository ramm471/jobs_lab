import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobs_lab/core/navigation/listener/job_id_listener.dart';
import 'package:jobs_lab/core/service/jobs_service.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/style/text_style.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/core/widget/progress_widget.dart';
import 'package:jobs_lab/core/widget/snack_bar.dart';
import 'package:jobs_lab/screens/job/data/cache/commitments_cache.dart';
import 'package:jobs_lab/screens/job/data/cache/companies_cache.dart';
import 'package:jobs_lab/screens/job/data/repository/job_parser.dart';
import 'package:jobs_lab/screens/job/data/repository/job_repository.dart';
import 'package:jobs_lab/screens/job/domain/job_interactor.dart';
import 'package:jobs_lab/screens/job/domain/repository/job_repository.dart';
import 'package:jobs_lab/screens/job/presentation/widget/job_widget.dart';
import 'package:jobs_lab/screens/jobs/data/cache/jobs_cache.dart';
import 'package:jobs_lab/screens/jobs/data/repository/jobs_parser.dart';
import 'package:jobs_lab/screens/jobs/data/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/domain/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/presentation/bloc/refresh_listener.dart';
import 'package:provider/provider.dart';

import 'bloc/job_cubit.dart';
import 'bloc/job_state.dart';

class JobScreen extends StatelessWidget {
  final String _id;

  const JobScreen({
    required Key key,
    required String id,
  })  : _id = id,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsScreen build");
    return MultiProvider(
      providers: [
        Provider<JobsParser>(
          create: (context) => JobsParserImpl(),
        ),
        Provider<JobsCache>(
          create: (context) => JobsCacheImpl(),
        ),
        Provider<JobParser>(
          create: (context) => JobParserImpl(),
        ),
        Provider<CommitmentsCache>(
          create: (context) => CommitmentsCacheImpl(),
        ),
        Provider<CompaniesCache>(
          create: (context) => CompaniesCacheImpl(),
        ),
        Provider<JobsRepository>(
          create: (context) => JobsRepositoryImpl(
            service: context.read<JobsService>(),
            jobsCache: context.read<JobsCache>(),
            parser: context.read<JobsParser>(),
          ),
        ),
        Provider<JobRepository>(
          create: (context) => JobRepositoryImpl(
            service: context.read<JobsService>(),
            parser: context.read<JobParser>(),
            commitmentsCache: context.read<CommitmentsCache>(),
            companiesCache: context.read<CompaniesCache>(),
          ),
        ),
        Provider<JobInteractor>(
          create: (context) => JobInteractorImpl(
            jobsRepository: context.read<JobsRepository>(),
            jobRepository: context.read<JobRepository>(),
            jobsCache: context.read<JobsCache>(),
            refreshListener: context.read<JobsRefreshListener>(),
          ),
        ),
      ],
      child: BlocProvider(
        key: JobsLabKeys.jobBlocProviderKey(_id),
        create: (context) => JobCubit(
          id: _id,
          jobInteractor: context.read<JobInteractor>(),
        ),
        child: JobWidgetScreen(
          key: JobsLabKeys.jobScreenWidgetKey(_id),
          id: _id,
        ),
      ),
    );
  }
}

class JobWidgetScreen extends StatelessWidget {
  final String _id;

  const JobWidgetScreen({
    required Key key,
    required String id,
  })  : _id = id,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobWidgetScreen build");
    return Scaffold(
      key: JobsLabKeys.jobScaffoldKey(_id),
      appBar: AppBar(
        key: JobsLabKeys.jobAppBarKey(_id),
        title: Text(
          _id.isEmpty
              ? (JobsLabLocalizations.of(context)?.jobPublishTitleText).orEmpty
              : (JobsLabLocalizations.of(context)?.jobUpdateTitleText).orEmpty,
          key: JobsLabKeys.jobAppBarTextKey(_id),
          style: JobsLabTextStyle.headline4AccentTextStyle(context),
        ),
      ),
      body: BlocConsumer<JobCubit, JobState>(
        key: JobsLabKeys.jobBlocKey(_id),
        listener: (context, state) {
          debugPrint("JobWidgetScreen Consumer state: $state");
          if (state is JobAddedState) {
            SnackBarHelper.showSuccess(context, (JobsLabLocalizations.of(context)?.success).orEmpty);
            context.read<JobIdListener>().selectValue(null);
          }
        },
        builder: (context, state) {
          debugPrint("JobWidgetScreen builder state: $state");
          if (state is JobLoadingState) {
            return const Center(
              child: CircularProgressWidget(),
            );
          } else if (state is JobErrorState) {
            return Center(
              child: Text(
                state.error ?? (JobsLabLocalizations.of(context)?.error).orEmpty,
                style: JobsLabTextStyle.headline4ErrorTextStyle(context),
              ),
            );
          } else if (state is JobLoadedState) {
            return JobWidget(
              key: JobsLabKeys.jobWidgetKey(_id),
              job: state.job,
              commitments: state.commitments,
              companies: state.companies,
              isSaveButtonEnabled: state.isSaveButtonEnabled,
            );
          } else {
            return const SizedBox(
              width: 0,
              height: 0,
            );
          }
        },
      ),
    );
  }
}
