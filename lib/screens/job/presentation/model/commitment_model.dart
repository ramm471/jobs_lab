import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jobs_lab/screens/jobs/data/entity/commitment_entity.dart';
import 'package:uuid/uuid.dart';

part 'commitment_model.freezed.dart';

@Freezed()
class CommitmentModel with _$CommitmentModel {
  const factory CommitmentModel({
    required String id,
    required String title,
  }) = _CommitmentModel;

  static CommitmentModel fromEntity(CommitmentEntity entity) {
    return CommitmentModel(
      id: entity.id,
      title: entity.title,
    );
  }

  static CommitmentEntity toEntity(CommitmentModel? model) {
    return CommitmentEntity(
      id: model?.id ?? const Uuid().v4(),
      title: model?.title ?? "",
    );
  }
}
