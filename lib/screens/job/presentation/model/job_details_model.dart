import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';
import 'package:uuid/uuid.dart';

import 'commitment_model.dart';
import 'company_model.dart';

part 'job_details_model.freezed.dart';

@Freezed()
class JobDetailsModel with _$JobDetailsModel {
  const factory JobDetailsModel({
    String? id,
    String? title,
    CommitmentModel? commitment,
    String? description,
    String? location,
    String? userEmail,
    String? applyUrl,
    CompanyModel? company,
    bool? isPending,
  }) = _JobDetailsModel;

  static JobDetailsModel fromEntity(JobEntity entity) {
    return JobDetailsModel(
      id: entity.id,
      title: entity.title,
      commitment: CommitmentModel.fromEntity(entity.commitment),
      description: entity.description,
      location: entity.location,
      userEmail: entity.userEmail,
      applyUrl: entity.applyUrl,
      company: CompanyModel.fromEntity(entity.company),
      isPending: entity.isPending,
    );
  }

  static JobEntity toEntity(JobDetailsModel model) {
    return JobEntity(
      id: model.id ?? const Uuid().v4(),
      title: model.title ?? "",
      description: model.description ?? "",
      commitment: CommitmentModel.toEntity(model.commitment),
      location: model.location,
      userEmail: model.userEmail,
      applyUrl: model.applyUrl,
      company: CompanyModel.toEntity(model.company),
      isPending: model.isPending,
    );
  }
}

extension IsEmptyExtensions on JobDetailsModel {
  bool get isNotEmpty =>
      title?.isNotEmpty == true &&
      commitment != null &&
      description?.isNotEmpty == true &&
      location?.isNotEmpty == true &&
      userEmail?.isNotEmpty == true &&
      applyUrl?.isNotEmpty == true &&
      company != null;
}
