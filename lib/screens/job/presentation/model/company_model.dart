import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jobs_lab/screens/jobs/data/entity/company_entity.dart';

part 'company_model.freezed.dart';

@Freezed()
class CompanyModel with _$CompanyModel {
  const factory CompanyModel({
    required String id,
    required String title,
  }) = _CompanyModel;

  static CompanyModel? fromEntity(CompanyEntity? entity) {
    if (entity == null) return null;
    return CompanyModel(
      id: entity.id,
      title: entity.title,
    );
  }

  static CompanyEntity? toEntity(CompanyModel? model) {
    if (model == null) return null;
    return CompanyEntity(
      id: model.id,
      title: model.title,
    );
  }
}
