import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/widget/text_form_input_decoration.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:provider/src/provider.dart';

class UrlWidget extends StatelessWidget {
  final String? _url;

  const UrlWidget({
    required Key key,
    required String url,
  })  : _url = url,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("UrlWidget build");
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 6,
      ),
      child: TextFormField(
        key: JobsLabKeys.jobUrlKey,
        initialValue: _url,
        keyboardType: TextInputType.url,
        decoration: TextFormInputDecoration(
          hintText: JobsLabLocalizations.of(context)?.urlHint,
        ),
        validator: (val) {
          return (val?.trim().isEmpty ?? true) ? JobsLabLocalizations.of(context)?.urlValidationError : null;
        },
        onChanged: (value) {
          debugPrint("UrlWidget onChanged $value");
          context.read<JobCubit>().selectUrl(value);
        },
      ),
    );
  }
}
