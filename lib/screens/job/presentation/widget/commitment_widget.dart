import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:provider/src/provider.dart';

class CommitmentWidget extends StatelessWidget {
  final List<CommitmentModel> _commitments;
  final CommitmentModel? _selectedCommitment;

  const CommitmentWidget({
    required Key key,
    required List<CommitmentModel> commitments,
    required CommitmentModel? selectedCommitment,
  })  : _commitments = commitments,
        _selectedCommitment = selectedCommitment,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("CommitmentWidget build");
    return DropdownButton<CommitmentModel>(
      key: JobsLabKeys.jobCommitmentDropDownKey,
      isExpanded: true,
      value: _selectedCommitment,
      items: _commitments
          .map<DropdownMenuItem<CommitmentModel>>((CommitmentModel value) => DropdownMenuItem<CommitmentModel>(
                value: value,
                child: Text(
                  value.title,
                ),
              ))
          .toList(),
      hint: Text(
        (JobsLabLocalizations.of(context)?.commitmentHint).orEmpty,
      ),
      onChanged: (CommitmentModel? value) {
        debugPrint("CommitmentWidget onChanged $value");
        if (value != null) {
          context.read<JobCubit>().selectCommitment(value);
        }
      },
    );
  }
}
