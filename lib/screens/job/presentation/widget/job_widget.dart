import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/job_details_model.dart';
import 'package:provider/src/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'job_inputs_large_widget.dart';
import 'job_inputs_widget.dart';

class JobWidget extends StatelessWidget {
  final JobDetailsModel _job;
  final List<CommitmentModel> _commitments;
  final List<CompanyModel> _companies;
  final bool _isSaveButtonEnabled;
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  const JobWidget({
    required Key key,
    required JobDetailsModel job,
    required List<CommitmentModel> commitments,
    required List<CompanyModel> companies,
    required bool isSaveButtonEnabled,
  })  : _job = job,
        _commitments = commitments,
        _companies = companies,
        _isSaveButtonEnabled = isSaveButtonEnabled,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobWidget build");
    return Padding(
      padding: const EdgeInsets.all(16),
      child: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          final isLarge = sizingInformation.deviceScreenType == DeviceScreenType.desktop ||
              sizingInformation.deviceScreenType == DeviceScreenType.tablet;
          debugPrint("JobsSliverWidget ResponsiveBuilder isLarge: $isLarge");
          return Stack(
            children: [
              SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: sizingInformation.screenSize.height,
                    minWidth: sizingInformation.screenSize.width,
                  ),
                  child: Form(
                    key: _formKey,
                    child: isLarge
                        ? JobInputsLargeWidget(
                            key: JobsLabKeys.jobInputsWidgetKey(
                              _job.id.orEmpty,
                            ),
                            job: _job,
                            commitments: _commitments,
                            companies: _companies,
                          )
                        : JobInputsWidget(
                            key: JobsLabKeys.jobInputsWidgetKey(
                              _job.id.orEmpty,
                            ),
                            job: _job,
                            commitments: _commitments,
                            companies: _companies,
                          ),
                  ),
                ),
              ),
              Container(
                width: sizingInformation.screenSize.width,
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: sizingInformation.screenSize.width,
                  color: JobsLabColors.backgroundColor,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: JobsLabColors.accentColor,
                      onPrimary: JobsLabColors.textPrimaryColor,
                      onSurface: JobsLabColors.disabledColor,
                      elevation: 5,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 8,
                      ),
                    ),
                    onPressed: _isSaveButtonEnabled
                        ? () {
                      if (_formKey.currentState?.validate() ?? false) {
                        context.read<JobCubit>().save();
                      }
                    }
                        : null,
                    child: Text(
                      _job.id.orEmpty.isEmpty
                          ? (JobsLabLocalizations.of(context)?.publishButtonText).orEmpty
                          : (JobsLabLocalizations.of(context)?.saveButtonText).orEmpty,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
