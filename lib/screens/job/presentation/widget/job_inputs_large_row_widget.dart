import 'package:flutter/material.dart';

class JobInputsLargeRowWidget extends StatelessWidget {
  final List<Widget> _firstChildren;
  final List<Widget> _secondChildren;

  const JobInputsLargeRowWidget({
    Key? key,
    required List<Widget> firstChildren,
    required List<Widget> secondChildren,
  })  : _firstChildren = firstChildren,
        _secondChildren = secondChildren,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobInputsLargeRowWidget build");
    final screenWidth = MediaQuery.of(context).size.width;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: screenWidth / 2.1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: _firstChildren,
          ),
        ),
        SizedBox(
          width: screenWidth / 2.1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: _secondChildren,
          ),
        ),
      ],
    );
  }
}
