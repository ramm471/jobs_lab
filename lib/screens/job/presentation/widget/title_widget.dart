import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/widget/text_form_input_decoration.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:provider/src/provider.dart';

class TitleWidget extends StatelessWidget {
  final String? _title;

  const TitleWidget({
    required Key key,
    required String title,
  })  : _title = title,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("TitleWidget build");
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 6,
      ),
      child: TextFormField(
        key: JobsLabKeys.jobTitleKey,
        initialValue: _title,
        keyboardType: TextInputType.text,
        decoration: TextFormInputDecoration(
          hintText: JobsLabLocalizations.of(context)?.titleHint,
        ),
        validator: (val) {
          return (val?.trim().isEmpty ?? true) ? JobsLabLocalizations.of(context)?.titleValidationError : null;
        },
        onChanged: (value) {
          debugPrint("TitleWidget onChanged $value");
          context.read<JobCubit>().selectTitle(value);
        },
      ),
    );
  }
}
