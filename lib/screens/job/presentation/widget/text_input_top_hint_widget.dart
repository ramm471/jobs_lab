import 'package:flutter/material.dart';

class TextInputTopHint extends StatelessWidget {
  final String _hint;

  const TextInputTopHint({
    Key? key,
    required String hint,
  })  : _hint = hint,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("TextInputTopHint build");
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text(
        _hint,
      ),
    );
  }
}
