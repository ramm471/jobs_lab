import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/widget/text_form_input_decoration.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:provider/src/provider.dart';

class EmailWidget extends StatelessWidget {
  final String? _email;
  final RegExp _emailReg;

  EmailWidget({
    required Key key,
    required String email,
    RegExp? emailReg,
  })  : _email = email,
        _emailReg = emailReg ?? RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("EmailWidget build");
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 6,
      ),
      child: TextFormField(
        key: JobsLabKeys.jobEmailKey,
        initialValue: _email,
        keyboardType: TextInputType.emailAddress,
        decoration: TextFormInputDecoration(
          hintText: JobsLabLocalizations.of(context)?.emailHint,
        ),
        validator: (val) {
          return ((val?.trim().isEmpty ?? true) || !(_emailReg.hasMatch(val?.trim() ?? "")))
              ? JobsLabLocalizations.of(context)?.emailValidationError
              : null;
        },
        onChanged: (value) {
          debugPrint("EmailWidget onChanged $value");
          context.read<JobCubit>().selectEmail(value);
        },
      ),
    );
  }
}
