import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/job_details_model.dart';
import 'package:jobs_lab/screens/job/presentation/widget/text_input_top_hint_widget.dart';
import 'package:jobs_lab/screens/job/presentation/widget/title_widget.dart';
import 'package:jobs_lab/screens/job/presentation/widget/url_widget.dart';

import 'commitment_widget.dart';
import 'companies_widget.dart';
import 'description_widget.dart';
import 'email_widget.dart';
import 'job_inputs_large_row_widget.dart';
import 'location_widget.dart';

class JobInputsLargeWidget extends StatelessWidget {
  final JobDetailsModel _job;
  final List<CommitmentModel> _commitments;
  final List<CompanyModel> _companies;

  const JobInputsLargeWidget({
    required Key key,
    required JobDetailsModel job,
    required List<CommitmentModel> commitments,
    required List<CompanyModel> companies,
  })  : _job = job,
        _commitments = commitments,
        _companies = companies,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobInputsLargeWidget build");
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        JobInputsLargeRowWidget(
          firstChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobTitleHint).orEmpty,
            ),
            TitleWidget(
              key: JobsLabKeys.jobTitleWidgetKey(_job.id.orEmpty),
              title: _job.title.orEmpty,
            ),
          ],
          secondChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobLocationHint).orEmpty,
            ),
            LocationWidget(
              key: JobsLabKeys.jobLocationWidgetKey(_job.id.orEmpty),
              location: _job.location.orEmpty,
            ),
          ],
        ),
        JobInputsLargeRowWidget(
          firstChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobEmailHint).orEmpty,
            ),
            EmailWidget(
              key: JobsLabKeys.jobEmailWidgetKey(_job.id.orEmpty),
              email: _job.userEmail.orEmpty,
            ),
          ],
          secondChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobApplyUrlHint).orEmpty,
            ),
            UrlWidget(
              key: JobsLabKeys.jobUrlWidgetKey(_job.id.orEmpty),
              url: _job.applyUrl.orEmpty,
            ),
          ],
        ),
        JobInputsLargeRowWidget(
          firstChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobCommitmentHint).orEmpty,
            ),
            CommitmentWidget(
              key: JobsLabKeys.jobCommitmentKey(_job.id.orEmpty),
              commitments: _commitments,
              selectedCommitment: _job.commitment,
            ),
          ],
          secondChildren: [
            TextInputTopHint(
              hint: (JobsLabLocalizations.of(context)?.jobCompanyHint).orEmpty,
            ),
            CompaniesWidget(
              key: JobsLabKeys.jobCompaniesKey(_job.id.orEmpty),
              companies: _companies,
              selectedCompany: _job.company,
            ),
          ],
        ),
        TextInputTopHint(
          hint: (JobsLabLocalizations.of(context)?.jobDescriptionHint).orEmpty,
        ),
        DescriptionWidget(
          key: JobsLabKeys.jobDescriptionWidgetKey(_job.id.orEmpty),
          description: _job.description.orEmpty,
        ),
        const SizedBox(
          width: 0,
          height: 60,
        )
      ],
    );
  }
}
