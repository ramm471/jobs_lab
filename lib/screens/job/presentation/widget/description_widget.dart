import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/widget/text_form_input_decoration.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:provider/src/provider.dart';

class DescriptionWidget extends StatelessWidget {
  final String? _description;

  const DescriptionWidget({
    required Key key,
    required String description,
  })  : _description = description,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("DescriptionWidget build");
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 6,
      ),
      child: TextFormField(
        key: JobsLabKeys.jobDescriptionKey,
        initialValue: _description,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        maxLengthEnforcement: MaxLengthEnforcement.truncateAfterCompositionEnds,
        decoration: TextFormInputDecoration(
          hintText: JobsLabLocalizations.of(context)?.descriptionHint,
        ),
        validator: (val) {
          return (val?.trim().isEmpty ?? true) ? JobsLabLocalizations.of(context)?.descriptionValidationError : null;
        },
        onChanged: (value) {
          debugPrint("DescriptionWidget onChanged $value");
          context.read<JobCubit>().selectDescription(value);
        },
      ),
    );
  }
}
