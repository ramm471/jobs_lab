import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/widget/text_form_input_decoration.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:provider/src/provider.dart';

class LocationWidget extends StatelessWidget {
  final String? _location;

  const LocationWidget({
    required Key key,
    required String location,
  })  : _location = location,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("LocationWidget build");
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 6,
      ),
      child: TextFormField(
        key: JobsLabKeys.jobLocationKey,
        initialValue: _location,
        keyboardType: TextInputType.text,
        decoration: TextFormInputDecoration(
          hintText: JobsLabLocalizations.of(context)?.locationHint,
        ),
        validator: (val) {
          return (val?.trim().isEmpty ?? true) ? JobsLabLocalizations.of(context)?.locationValidationError : null;
        },
        onChanged: (value) {
          debugPrint("LocationWidget onChanged $value");
          context.read<JobCubit>().selectLocation(value);
        },
      ),
    );
  }
}
