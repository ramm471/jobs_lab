import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/job/presentation/bloc/job_cubit.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:provider/src/provider.dart';

class CompaniesWidget extends StatelessWidget {
  final List<CompanyModel> _companies;
  final CompanyModel? _selectedCompany;

  const CompaniesWidget({
    required Key key,
    required List<CompanyModel> companies,
    required CompanyModel? selectedCompany,
  })  : _companies = companies,
        _selectedCompany = selectedCompany,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("CompaniesWidget build");
    return DropdownButton<CompanyModel>(
      key: JobsLabKeys.jobCompaniesDropDownKey,
      isExpanded: true,
      value: _selectedCompany,
      items: _companies
          .map<DropdownMenuItem<CompanyModel>>((CompanyModel value) => DropdownMenuItem<CompanyModel>(
                value: value,
                child: Text(
                  value.title,
                ),
              ))
          .toList(),
      hint: Text(
        (JobsLabLocalizations.of(context)?.companyHint).orEmpty,
      ),
      onChanged: (CompanyModel? value) {
        debugPrint("CompaniesWidget onChanged $value");
        if (value != null) {
          context.read<JobCubit>().selectCompany(value);
        }
      },
    );
  }
}
