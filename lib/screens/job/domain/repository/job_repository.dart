import 'dart:core';

import 'package:jobs_lab/screens/jobs/data/entity/commitment_entity.dart';
import 'package:jobs_lab/screens/jobs/data/entity/company_entity.dart';

abstract class JobRepository {
  Future<List<CommitmentEntity>> loadCommitments();

  Future<List<CompanyEntity>> loadCompanies();
}
