import 'package:flutter/cupertino.dart';
import 'package:jobs_lab/screens/job/domain/repository/job_repository.dart';
import 'package:jobs_lab/screens/job/presentation/model/commitment_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/company_model.dart';
import 'package:jobs_lab/screens/job/presentation/model/job_details_model.dart';
import 'package:jobs_lab/screens/jobs/data/cache/jobs_cache.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';
import 'package:jobs_lab/screens/jobs/domain/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/presentation/bloc/refresh_listener.dart';

abstract class JobInteractor {
  Future<JobDetailsModel> loadJob(String id);

  Future<List<CommitmentModel>> loadCommitments();

  Future<List<CompanyModel>> loadCompanies();

  Future<void> addPending(JobEntity entity);
}

class JobInteractorImpl implements JobInteractor {
  final JobsCache _jobsCache;
  final JobsRepository _jobsRepository;
  final JobRepository _jobRepository;
  final JobsRefreshListener _jobsRefreshListener;

  JobInteractorImpl({
    required JobsCache jobsCache,
    required JobsRepository jobsRepository,
    required JobRepository jobRepository,
    required JobsRefreshListener refreshListener,
  })  : _jobsCache = jobsCache,
        _jobsRepository = jobsRepository,
        _jobRepository = jobRepository,
        _jobsRefreshListener = refreshListener;

  @override
  Future<JobDetailsModel> loadJob(String id) async {
    debugPrint("loadJob");
    if (id.isNotEmpty) {
      final jobs = await _jobsRepository.loadJobs();
      final job = jobs.firstWhere((element) => element.id == id);
      return JobDetailsModel.fromEntity(job);
    } else {
      return JobDetailsModel(id: id);
    }
  }

  @override
  Future<List<CommitmentModel>> loadCommitments() async {
    debugPrint("_loadCommitments");
    final commitments = await _jobRepository.loadCommitments();
    return commitments.map((e) => CommitmentModel.fromEntity(e)).toList();
  }

  @override
  Future<List<CompanyModel>> loadCompanies() async {
    debugPrint("_loadCompanies");
    final entities = await _jobRepository.loadCompanies();
    return entities.map((e) => CompanyModel.fromEntity(e)).whereType<CompanyModel>().toList();
  }

  @override
  Future<void> addPending(JobEntity entity) async {
    final cachedEntities = await _jobsCache.getEntities();
    final updated = cachedEntities.toList()
      ..removeWhere((element) => element.id == entity.id)
      ..add(entity);
    await _jobsCache.setEntities(updated);
    _jobsRefreshListener.selectValue(true);
  }
}
