import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:graphql/client.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';

abstract class JobsParser {
  Future<List<JobEntity>> parseJobsResponse(QueryResult result);

  Future<JobEntity> parseJobResponse(QueryResult result);
}

class JobsParserImpl implements JobsParser {
  @override
  Future<List<JobEntity>> parseJobsResponse(QueryResult result) async {
    debugPrint("parseJobsResponse");
    final List<JobEntity> entities = [];
    if (result.hasException) {
      debugPrint("parseJobsResponse error ${result.exception?.toString()} error: ${result.exception}");
      throw Exception("parseJobsResponse error ${result.exception?.toString()}");
    } else {
      debugPrint("parseJobsResponse parsing");
      final jobsJson = result.data?['jobs'] as List<dynamic>?;
      final List<JobEntity> parsedJobs =
          jobsJson?.map((e) => _tryParseJobJson(e)).whereType<JobEntity>().toList() ?? [];
      entities.addAll(parsedJobs);
      debugPrint("parseJobsResponse entities size: ${entities.length}");
    }
    return entities;
  }

  @override
  Future<JobEntity> parseJobResponse(QueryResult result) async {
    debugPrint("parseJobResponse");
    if (result.hasException) {
      debugPrint("parseJobResponse error ${result.exception?.toString()} error: ${result.exception}");
      throw Exception("parseJobResponse error ${result.exception?.toString()}");
    } else {
      debugPrint("parseJobResponse parsing");
      final jobJson = result.data?['postJob'] as dynamic;
      final JobEntity? parsedJob = _tryParseJobJson(jobJson);
      debugPrint("parseJobResponse entity: $parsedJob");
      if (parsedJob != null) {
        return parsedJob;
      } else {
        throw Exception("parseJobResponse error ${result.exception?.toString()}");
      }
    }
  }

  JobEntity? _tryParseJobJson(dynamic value) {
    try {
      return JobEntity.fromJson(Map<String, dynamic>.from(value));
    } catch (e) {
      debugPrint("_tryParseJobJson error: $e");
      return null;
    }
  }
}
