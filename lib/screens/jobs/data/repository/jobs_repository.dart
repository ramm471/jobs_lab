import 'dart:core';

import 'package:flutter/foundation.dart' show debugPrint;
import 'package:graphql/client.dart';
import 'package:jobs_lab/core/service/jobs_service.dart';
import 'package:jobs_lab/screens/jobs/data/cache/jobs_cache.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';
import 'package:jobs_lab/screens/jobs/domain/repository/jobs_repository.dart';

import 'jobs_parser.dart';

class JobsRepositoryImpl implements JobsRepository {
  final JobsService _service;
  final JobsCache _jobsCache;
  final JobsParser _parser;

  JobsRepositoryImpl({
    required JobsService service,
    required JobsCache jobsCache,
    required JobsParser parser,
  })  : _service = service,
        _jobsCache = jobsCache,
        _parser = parser;

  @override
  Future<List<JobEntity>> loadJobs() async {
    debugPrint("loadJobs");

    final cachedEntities = await _jobsCache.getEntities();
    if (cachedEntities.isNotEmpty) {
      return cachedEntities;
    } else {
      final QueryResult result = await _service.loadJobs();
      final entities = await _parser.parseJobsResponse(result);
      _jobsCache.setEntities(entities);
      debugPrint("loadJobs return ${entities.length} entities");
      return entities;
    }
  }

  @override
  Future<bool> pushJobs(List<JobEntity> entities) async {
    final List<Exception> errors = [];
    final cachedEntities = await _jobsCache.getEntities();
    for (var entity in entities) {
      final result = await _service.pushJob(JobEntity.toPostJobMap(entity));
      try {
        final pushedEntity = await _parser.parseJobResponse(result);
        cachedEntities.removeWhere((element) => element == entity);
        cachedEntities.insert(0, pushedEntity);
      } on Exception catch (e) {
        errors.add(e);
      }
    }
    await _jobsCache.setEntities(cachedEntities);
    debugPrint("pushJobs return ${errors.isEmpty}");
    if (errors.isNotEmpty) {
      debugPrint("pushJobs error ${errors[0].toString()} error: ${errors[0]}");
      throw Exception(errors[0]);
    }
    return errors.isEmpty;
  }
}
