import 'dart:core';

import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';

import '../../../../core/cache/base_cache.dart';

abstract class JobsCache extends BaseCacheImpl<JobEntity> {
  JobsCache()
      : super(
          entityKey: _jobsKey,
          fromJson: (e) => JobEntity.fromJson(e),
        );
}

class JobsCacheImpl extends JobsCache {}

const String _jobsKey = "_JOBS_KEY";
