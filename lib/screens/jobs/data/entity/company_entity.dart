import 'package:freezed_annotation/freezed_annotation.dart';

part 'company_entity.freezed.dart';

part 'company_entity.g.dart';

@Freezed()
class CompanyEntity with _$CompanyEntity {
  @JsonSerializable(explicitToJson: true)
  const factory CompanyEntity({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'name') required String title,
  }) = _CompanyEntity;

  factory CompanyEntity.fromJson(Map<String, dynamic> json) => _$CompanyEntityFromJson(json);
}
