import 'package:freezed_annotation/freezed_annotation.dart';

import 'commitment_entity.dart';
import 'company_entity.dart';

part 'job_entity.freezed.dart';

part 'job_entity.g.dart';

@Freezed()
class JobEntity with _$JobEntity {
  @JsonSerializable(explicitToJson: true)
  const factory JobEntity(
      {@JsonKey(name: 'id') required String id,
      @JsonKey(name: 'title') required String title,
      @JsonKey(name: 'description') required String description,
      @JsonKey(name: 'commitment') required CommitmentEntity commitment,
      @JsonKey(name: 'locationNames') String? location,
      @JsonKey(name: 'userEmail') String? userEmail,
      @JsonKey(name: 'applyUrl') String? applyUrl,
      @JsonKey(name: 'company') CompanyEntity? company,
      bool? isPending}) = _JobEntity;

  factory JobEntity.fromJson(Map<String, dynamic> json) => _$JobEntityFromJson(json);

  static Map<String, dynamic> toPostJobMap(JobEntity entity) {
    return {
      "title": entity.title,
      "commitmentId": entity.commitment.id,
      "companyName": entity.company?.title ?? "",
      "locationNames": entity.location ?? "",
      "userEmail": entity.userEmail ?? "",
      "description": entity.description,
      "applyUrl": entity.applyUrl ?? "",
    };
  }
}
