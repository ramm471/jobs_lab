import 'package:freezed_annotation/freezed_annotation.dart';

part 'commitment_entity.freezed.dart';

part 'commitment_entity.g.dart';

@Freezed()
class CommitmentEntity with _$CommitmentEntity {
  @JsonSerializable(explicitToJson: true)
  const factory CommitmentEntity({
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'title') required String title,
  }) = _CommitmentEntity;

  factory CommitmentEntity.fromJson(Map<String, dynamic> json) => _$CommitmentEntityFromJson(json);
}
