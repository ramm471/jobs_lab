import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';

part 'job_model.freezed.dart';

@Freezed()
class JobModel with _$JobModel {
  const factory JobModel({
    required String id,
    required String title,
    required String description,
    required bool isPending,
  }) = _JobModel;

  static JobModel fromEntity(JobEntity entity) {
    return JobModel(
      id: entity.id,
      title: entity.title,
      description: entity.description,
      isPending: entity.isPending ?? false,
    );
  }
}
