import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobs_lab/core/navigation/listener/job_id_listener.dart';
import 'package:jobs_lab/core/service/jobs_service.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/style/text_style.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/core/widget/progress_widget.dart';
import 'package:jobs_lab/core/widget/snack_bar.dart';
import 'package:jobs_lab/screens/jobs/data/cache/jobs_cache.dart';
import 'package:jobs_lab/screens/jobs/data/repository/jobs_parser.dart';
import 'package:jobs_lab/screens/jobs/data/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/domain/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/presentation/widget/jobs_sliver_widget.dart';
import 'package:provider/provider.dart';

import 'bloc/jobs_cubit.dart';
import 'bloc/jobs_state.dart';
import 'bloc/refresh_listener.dart';

class JobsScreen extends StatelessWidget {
  const JobsScreen({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsScreen build");
    return MultiProvider(
      providers: [
        Provider<JobsParser>(
          create: (context) => JobsParserImpl(),
        ),
        Provider<JobsCache>(
          create: (context) => JobsCacheImpl(),
        ),
        Provider<JobsRepository>(
          create: (context) => JobsRepositoryImpl(
            service: context.read<JobsService>(),
            jobsCache: context.read<JobsCache>(),
            parser: context.read<JobsParser>(),
          ),
        ),
      ],
      child: BlocProvider(
        key: JobsLabKeys.jobsBlocProviderKey,
        create: (context) => JobsCubit(
          repository: context.read<JobsRepository>(),
          refreshListener: context.read<JobsRefreshListener>(),
        )..loadJobs(),
        child: const JobsScreenWidget(
          key: JobsLabKeys.jobsScreenWidgetKey,
        ),
      ),
    );
  }
}

class JobsScreenWidget extends StatelessWidget {
  const JobsScreenWidget({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsScreenWidget build");
    return Scaffold(
      key: JobsLabKeys.jobsScaffoldKey,
      appBar: AppBar(
        key: JobsLabKeys.jobsAppBarKey,
        title: Text(
          (JobsLabLocalizations.of(context)?.appTitle).orEmpty,
          key: JobsLabKeys.jobsAppBarTextKey,
          style: JobsLabTextStyle.headline4AccentTextStyle(context),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: GestureDetector(
              onTap: () {
                context.read<JobIdListener>().selectValue("");
              },
              child: const Icon(
                Icons.add,
                size: 26.0,
              ),
            ),
          ),
        ],
      ),
      body: BlocConsumer<JobsCubit, JobsState>(
        key: JobsLabKeys.jobsBlocKey,
        listener: (context, state) {
          if (state is JobsPublishedState) {
            SnackBarHelper.showSuccess(context, (JobsLabLocalizations.of(context)?.successPublish).orEmpty);
          } else if (state is JobsPublishFailedState) {
            SnackBarHelper.showError(context, (JobsLabLocalizations.of(context)?.error).orEmpty);
          }
        },
        buildWhen: (previous, current) {
          return current is! JobsPublishFailedState && current is! JobsPublishedState;
        },
        builder: (context, state) {
          debugPrint("JobsScreenWidget builder state: $state");
          if (state is JobsInitialState) {
            return Center(
              child: Text(
                (JobsLabLocalizations.of(context)?.jobsEmpty).orEmpty,
                style: JobsLabTextStyle.headline4ErrorTextStyle(context),
              ),
            );
          } else if (state is JobsLoadingState) {
            return const Center(
              child: CircularProgressWidget(),
            );
          } else if (state is JobsErrorState) {
            return Center(
              child: Text(
                state.error ?? (JobsLabLocalizations.of(context)?.error).orEmpty,
                style: JobsLabTextStyle.headline4ErrorTextStyle(context),
              ),
            );
          } else if (state is JobsLoadedState) {
            return JobsSliverWidget(
              key: JobsLabKeys.jobsSliverWidgetKey,
              jobs: state.jobs,
              pending: state.pending,
            );
          } else {
            return Center(
              child: Text(
                (JobsLabLocalizations.of(context)?.error).orEmpty,
                style: JobsLabTextStyle.headline4ErrorTextStyle(context),
              ),
            );
          }
        },
      ),
    );
  }
}
