import 'package:flutter/material.dart';
import 'package:jobs_lab/core/navigation/listener/job_id_listener.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/screens/jobs/presentation/model/job_model.dart';
import 'package:provider/src/provider.dart';

class JobsSliverGridItemWidget extends StatelessWidget {
  final List<JobModel> _jobs;

  const JobsSliverGridItemWidget({
    required Key key,
    required List<JobModel> jobs,
  })  : _jobs = jobs,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsSliverGridItemWidget build");
    final crossAxisCount = MediaQuery.of(context).size.width ~/ 350;
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        mainAxisSpacing: 0,
        crossAxisSpacing: 0,
        childAspectRatio: 3.0,
        crossAxisCount: crossAxisCount,
      ),
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          final item = _jobs[index];
          return GridTile(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                color: JobsLabColors.dividerColor,
                width: 0.5,
              )),
              child: ListTile(
                key: JobsLabKeys.jobsItem(item.id),
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                title: Padding(
                  padding: const EdgeInsets.only(
                    bottom: 6,
                  ),
                  child: Text(
                    item.title,
                    softWrap: true,
                    maxLines: 1,
                    key: JobsLabKeys.jobsItemTitle(item.id),
                  ),
                ),
                subtitle: Text(
                  item.description,
                  key: JobsLabKeys.jobsItemSubTitle(item.id),
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
                onTap: () {
                  debugPrint("onTap $item");
                  context.read<JobIdListener>().selectValue(item.id);
                },
              ),
            ),
          );
        },
        addAutomaticKeepAlives: true,
        childCount: _jobs.length,
      ),
    );
  }
}
