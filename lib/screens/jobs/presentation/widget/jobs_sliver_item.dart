import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jobs_lab/core/navigation/listener/job_id_listener.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/screens/jobs/presentation/model/job_model.dart';
import 'package:provider/src/provider.dart';

class JobsSliverItemWidget extends StatelessWidget {
  final List<JobModel> _jobs;

  const JobsSliverItemWidget({
    required Key key,
    required List<JobModel> jobs,
  })  : _jobs = jobs,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsSliverItemWidget build");
    return SliverList(
      delegate: SliverChildBuilderDelegate(
            (context, index) {
          final int itemIndex = index ~/ 2;
          if (index.isEven) {
            final item = _jobs[itemIndex];
            return ListTile(
              key: JobsLabKeys.jobsItem(item.id),
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              title: Padding(
                padding: const EdgeInsets.only(
                  bottom: 6,
                ),
                child: Text(
                  item.title,
                  key: JobsLabKeys.jobsItemTitle(item.id),
                ),
              ),
              subtitle: Text(
                item.description,
                key: JobsLabKeys.jobsItemSubTitle(item.id),
                maxLines: 3,
              ),
              onTap: () {
                debugPrint("onTap $item");
                context.read<JobIdListener>().selectValue(item.id);
              },
            );
          }
          return const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Divider(height: 1),
          );
        },
        semanticIndexCallback: (Widget widget, int localIndex) {
          if (localIndex.isEven) {
            return localIndex ~/ 2;
          }
          return null;
        },
        addAutomaticKeepAlives: true,
        childCount: max(0, _jobs.length * 2 - 1),
      ),
    );
  }
}
