import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/utils.dart';
import 'package:jobs_lab/screens/jobs/presentation/bloc/jobs_cubit.dart';
import 'package:provider/src/provider.dart';

class JobsSliverPendingItemWidget extends StatelessWidget {
  const JobsSliverPendingItemWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsSliverPendingItemWidget build");
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Container(
            decoration: BoxDecoration(color: JobsLabColors.primaryColorDark),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    (JobsLabLocalizations.of(context)?.pendingText).orEmpty,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    context.read<JobsCubit>().pushPendingJobs();
                  },
                  child: Text(
                    (JobsLabLocalizations.of(context)?.pushButtonText).orEmpty,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
