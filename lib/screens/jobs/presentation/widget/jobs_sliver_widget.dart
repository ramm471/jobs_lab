import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/keys.dart';
import 'package:jobs_lab/screens/jobs/presentation/model/job_model.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'jobs_sliver_grid_item.dart';
import 'jobs_sliver_item.dart';
import 'jobs_sliver_jobs_item.dart';
import 'jobs_sliver_pending_item.dart';

class JobsSliverWidget extends StatelessWidget {
  final List<JobModel> _jobs;
  final List<JobModel> _pending;

  const JobsSliverWidget({
    required Key key,
    required List<JobModel> jobs,
    required List<JobModel> pending,
  })  : _jobs = jobs,
        _pending = pending,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsSliverWidget build");
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        final isLarge = sizingInformation.deviceScreenType == DeviceScreenType.desktop ||
            sizingInformation.deviceScreenType == DeviceScreenType.tablet;
        debugPrint("JobsSliverWidget ResponsiveBuilder isLarge: $isLarge");
        return CustomScrollView(
          key: const PageStorageKey<String>('JobsSliverWidget'),
          slivers: [
            if (_pending.isNotEmpty) const JobsSliverPendingItemWidget(),
            if (_pending.isNotEmpty && !isLarge)
              JobsSliverItemWidget(
                key: JobsLabKeys.jobsSliverItemKey,
                jobs: _pending,
              ),
            if (_pending.isNotEmpty && isLarge)
              JobsSliverGridItemWidget(
                key: JobsLabKeys.jobsSliverItemKey,
                jobs: _pending,
              ),
            const JobsSliverJobsItemWidget(),
            if (!isLarge)
              JobsSliverItemWidget(
                key: JobsLabKeys.jobsSliverPendingItemKey,
                jobs: _jobs,
              ),
            if (isLarge)
              JobsSliverGridItemWidget(
                key: JobsLabKeys.jobsSliverPendingItemKey,
                jobs: _jobs,
              ),
          ],
        );
      },
    );
  }
}
