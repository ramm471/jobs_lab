import 'package:flutter/material.dart';
import 'package:jobs_lab/core/utils/localization/localization.dart';
import 'package:jobs_lab/core/utils/style/colors.dart';
import 'package:jobs_lab/core/utils/utils.dart';

class JobsSliverJobsItemWidget extends StatelessWidget {
  const JobsSliverJobsItemWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint("JobsSliverJobsItemWidget build");
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Container(
            decoration: BoxDecoration(color: JobsLabColors.primaryColorDark),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                (JobsLabLocalizations.of(context)?.jobsText).orEmpty,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
