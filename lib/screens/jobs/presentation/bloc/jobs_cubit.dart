import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';
import 'package:jobs_lab/screens/jobs/domain/repository/jobs_repository.dart';
import 'package:jobs_lab/screens/jobs/presentation/bloc/refresh_listener.dart';
import 'package:jobs_lab/screens/jobs/presentation/model/job_model.dart';

import 'jobs_state.dart';

class JobsCubit extends Cubit<JobsState> {
  final JobsRepository _repository;
  late final StreamSubscription? _jobIdListenerSubscription;

  JobsCubit({
    required JobsRepository repository,
    required JobsRefreshListener refreshListener,
  })  : _repository = repository,
        super(const JobsInitialState()) {
    _jobIdListenerSubscription = refreshListener.selectedValue.listen((refresh) {
      if (refresh ?? false) {
        loadJobs();
      }
    });
  }

  @override
  Future<void> close() async {
    _jobIdListenerSubscription?.cancel();
    super.close();
  }

  Future<void> loadJobs() async {
    debugPrint("loadJobs");
    if (state is JobsLoadingState) return;
    emit(const JobsLoadingState());

    final jobs = await _repository.loadJobs().onError((error, stackTrace) {
      emit(const JobsErrorState());
      return [];
    });
    debugPrint("_loadJobs ${jobs.length} items");
    if (jobs.isNotEmpty) {
      emit(JobsLoadedState(
        jobs: List.of(_mapToUIItems(jobs, false)),
        pending: List.of(_mapToUIItems(jobs, true)),
      ));
    }
  }

  Future<void> pushPendingJobs() async {
    final jobs = await _repository.loadJobs().onError((error, stackTrace) {
      emit(const JobsPublishFailedState());
      return [];
    });
    final pending = jobs.where((element) => element.isPending ?? false);
    final isSuccess = await _repository.pushJobs(pending.toList()).onError((error, stackTrace) {
      emit(const JobsPublishFailedState());
      return false;
    });
    debugPrint("pushPendingJobs isSuccess: $isSuccess");
    if (isSuccess) {
      emit(const JobsPublishedState());
      loadJobs();
    }
  }

  List<JobModel> _mapToUIItems(List<JobEntity> entities, bool isPending) {
    return entities.map((e) => JobModel.fromEntity(e)).where((e) => isPending ? e.isPending : !e.isPending).toList();
  }
}
