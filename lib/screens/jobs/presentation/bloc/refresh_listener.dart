import 'package:jobs_lab/core/navigation/listener/navigation_value_listener.dart';

abstract class JobsRefreshListener extends NavigationValueProviderImpl<bool> {}

class JobsRefreshListenerImpl extends JobsRefreshListener {}
