import 'package:equatable/equatable.dart';
import 'package:jobs_lab/screens/jobs/presentation/model/job_model.dart';

abstract class JobsState extends Equatable {
  const JobsState() : super();

  @override
  List<Object?> get props => [];
}

class JobsInitialState extends JobsState {
  const JobsInitialState() : super();
}

class JobsLoadingState extends JobsState {
  const JobsLoadingState() : super();
}

class JobsPublishedState extends JobsState {
  const JobsPublishedState() : super();
}

class JobsPublishFailedState extends JobsState {
  const JobsPublishFailedState() : super();
}

class JobsErrorState extends JobsState {
  final String? error;

  const JobsErrorState({this.error}) : super();

  @override
  List<Object?> get props => [error];

  @override
  String toString() {
    return 'JobsErrorState{error: $error}';
  }
}

class JobsLoadedState extends JobsState {
  final List<JobModel> jobs;
  final List<JobModel> pending;

  const JobsLoadedState({required this.jobs, required this.pending}) : super();

  @override
  List<Object> get props => [jobs, pending];

  @override
  String toString() {
    return 'JobsLoadedState{jobs: ${jobs.length} items, pending: $pending}';
  }
}
