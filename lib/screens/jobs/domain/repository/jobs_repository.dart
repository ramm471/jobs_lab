import 'dart:core';

import 'package:jobs_lab/screens/jobs/data/entity/job_entity.dart';

abstract class JobsRepository {
  Future<List<JobEntity>> loadJobs();

  Future<bool> pushJobs(List<JobEntity> entities);
}
